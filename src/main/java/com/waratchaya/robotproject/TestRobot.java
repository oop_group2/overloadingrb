/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.robotproject;

/**
 *
 * @author Melon
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(02, 06, 22, 17, 30);
        System.out.println(robot);
        robot.walk('N');
        System.out.println(robot);
        robot.walk('S');
        robot.walk('S');
        System.out.println(robot);
        robot.walk('E', 2);
        System.out.println(robot);
        robot.walk();
        System.out.println(robot);
        robot.walk(3);
        System.out.println(robot);
        robot.walk('E',14);
        System.out.println(robot);
        robot.walk('S',10);
        System.out.println(robot);
        robot.walk('N',100);
        System.out.println(robot);
    }
}
